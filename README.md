

一. 生成main.spec文件
   1. 指令  pyi-makespec -D main.py

二. path.txt 文件
 配置需打包上传的文件路径

三. config.ini 文件
  配置sftp、log等信息

二. 编译打包
   1. 指令  pyinstaller main.spec
   2. 将python 文件打包，生成dist文件，可执行文件路径为 dist/main/main
   3. 将config.ini、path.txt 打包到到dist文件中，文件在路径 dist/manage/ 下

